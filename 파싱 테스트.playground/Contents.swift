/**
 * Created by GyungDal on 2016.11.08
 **/
import Foundation

enum allergy : UInt8{
    case 난류 = 1
    , 유류
    , 메밀
    , 땅콩
    , 대두
    , 밀
    , 고등어
    , 게
    , 새우
    , 돼지고기
    , 복숭아
    , 토마토
    , 아황산염
    , 뭐
    , 냐
    , 이
    , 것
    , 들
    , 장
    , 난
    , 하
    , 나
}

enum mealType : UInt8{
    case BREAKFAST = 0
    , LUNCH
    , DINNER
    , NONE
}

struct Meal{
    var type : mealType
    var date : String
    var desc : String
    var allergies : Array<allergy>
    
    init() {
        type = mealType.NONE
        date = ""
        desc = ""
        allergies = Array<allergy>()
    }
    
    init(type : mealType, date : String, desc : String, allergies : Array<allergy>){
        self.type = type
        self.date = date
        self.desc = desc
        self.allergies = Array(Set(allergies))
        //중복값 제거
    }
    
        func getType() -> String {
        switch(self.type){
            case .BREAKFAST :
                return "아침"
            case .LUNCH :
                return "점심"
            case .DINNER :
                return "저녁"
            case .NONE :
                return "WTF???"
        }
    }
    
    func getAllergies() -> String {
        var result : String = ""
        for temp in self.allergies {
            result.append(String(describing: temp) + ", ")
        }
        return result
    }
}

import UIKit
import PlaygroundSupport

let currentPage = PlaygroundPage.current
currentPage.needsIndefiniteExecution = true

class downloader : NSObject, URLSessionDelegate, URLSessionDataDelegate {

    var session : URLSession!
    var tasks : [URLSessionDataTask : String] = [URLSessionDataTask : String]()

    func startDownloadWithDelegateHandler() {
        self.session = URLSession(configuration: URLSessionConfiguration.default, delegate: self, delegateQueue: OperationQueue.main)

        let urlString : String = < PUT YOUR URL HERE >

        guard let url = URL(string: urlString) else { return }
        let request : URLRequest = URLRequest(url: url)

        let dataTask : URLSessionDataTask = session.dataTask(with: request)
        self.tasks[dataTask] = urlString

        dataTask.resume()
    }

    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {
        print("Data received")
        print(dataTask.description)
    }

    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        if error != nil {
            print(error)
        }
        else
        {
            print("Finished")
            if let urlString = self.tasks[task as! URLSessionDataTask] {
                print(urlString)
            }
        }
    }

    func urlSession(_ session: URLSession, task: URLSessionTask, willPerformHTTPRedirection response: HTTPURLResponse, newRequest request: URLRequest, completionHandler: @escaping (URLRequest?) -> Void) {
        print("Getting redirected")
        print(response)

        let newDataTask = self.session.dataTask(with: request)
        self.tasks[newDataTask] = String(describing: request.url)
        print(newDataTask.taskDescription)
        newDataTask.resume()
    }

    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        completionHandler(URLSession.AuthChallengeDisposition.performDefaultHandling, nil)
    }

    func urlSession(_ session: URLSession, task: URLSessionTask, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        completionHandler(URLSession.AuthChallengeDisposition.performDefaultHandling, nil)
    }

    func urlSession(_ session: URLSession, didBecomeInvalidWithError error: Error?) {
        if let _ = error {
            print(error!)
        }
    }
}

let myDownloader = downloader()
myDownloader.startDownloadWithDelegateHandler()

class getMeal {
    private var html : String?
    private let pageUrl : String?
    private let DEBUG : Bool = true
    init(url : String){
        self.pageUrl = url
        getHtmlSource()
    }
    
    func getBreakfast() -> Array<Meal> {
        return getMeals(source : self.html!.components(separatedBy: "table")[4], type : mealType.BREAKFAST)
    }
    
    func getLunch() -> Array<Meal> {
        return getMeals(source : self.html!.components(separatedBy: "table")[9], type : mealType.LUNCH)
    }
    
    func getDinner() -> Array<Meal> {
        return getMeals(source : self.html!.components(separatedBy: "table")[14], type : mealType.DINNER)
    }
    
    
    private func getMeals(source : String, type : mealType) -> Array<Meal> {
        let temp = source.components(separatedBy: "tbody")[1]
        let times = temp.components(separatedBy: "</tr>")[0]
        print(times)
        
        let meals = temp.components(separatedBy: "</tr>")[1]
        
        var result = Array<Meal>()
        for i in 0...6 {
            let mealTemp = meals
                .components(separatedBy: "</td>")[i]
                .components(separatedBy: "\">")[1]
            
            var meal : String = ""
            var first : Bool = true
            var inFirst : Bool = true
            var alleriesTemp : Array<allergy> =  Array<allergy>()
            for var temp in mealTemp.components(separatedBy: "<br />"){
                if(first){
                    first = false
                    continue
                }else{
                    print ("TEMP : ", temp)
                    if(temp.range(of: ")") != nil){
                        //print("FOUND!")
                        var mealDesc : String = ""
                        inFirst = true
                        for t in temp.components(separatedBy: "("){
                            print(t)
                            if(inFirst){
                                mealDesc = t;
                                inFirst = false;
                            }else{
                                print(t)
                                if(UInt8(t.components(separatedBy: ")")[0]) != nil){
                                    print(UInt8(t.components(separatedBy: ")")[0])!)
                                    alleriesTemp.append(allergy.init(rawValue: UInt8(t.components(separatedBy: ")")[0])!)!)
                                }
                            }
                        }
                        temp = mealDesc
                    }
                    temp = temp.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).replacingOccurrences(of: "*", with: "\n")
                    if(temp.contains("철분")){
                        temp.append("\n")
                    }
                    meal.append(temp)

                }
            }
            result.append(Meal(type: type,
                               date: times
                                .components(separatedBy: "</td>")[i]
                                .components(separatedBy: "\">")[1]
                , desc: meal, allergies: alleriesTemp))
            
            
            
            if DEBUG{
                for debug in result {
                    print ("\n\n")
                    print ("DATE : " + debug.date)
                    print ("TYPE : " + debug.getType())
                    print ("DESC : " + debug.desc)
                    print ("ALLERGY : " + debug.getAllergies())
                    print ("DONE")
                }
            }
        }
        return result
    }
    
    private func getHtmlSource(){
        if let url = URL(string : self.pageUrl!){
            do{
                self.html = try String(contentsOf : url as URL)
            }catch{
                print(error)
            }
        }
    }
}


let test = getMeal(url: "http://dsm.hs.kr/segio/meal/meal.php?shell=/index.shell:55");
test.getBreakfast()
